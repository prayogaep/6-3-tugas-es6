// Soal 1
const k_persegi_panjang = (panjang, lebar) => {
    let keliling = 2 * (panjang + lebar)
    return keliling
}

// panggil Function
console.log(k_persegi_panjang(10, 5))
console.log('\n')


// Soal 2
// Ubahlah code dis bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }

const newFunction = (firstName, lastName) => 
{
    return {
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
    
}
//Driver Code 
newFunction("William", "Imoh").fullName() 
console.log('\n')



// Soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)


console.log('\n')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
//Driver Code
console.log(combined)


// Soal 5
const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 